package com.sencha.gxt.data.shared.loader;

/**
 * A {@link FilterHandler} that provides support for <code>String</code> values.
 */
public class StringFilterHandler extends FilterHandler<String> {

  @Override
  public String convertToObject(String value) {
    return value;
  }

  @Override
  public String convertToString(String object) {
    return object;
  }

}
