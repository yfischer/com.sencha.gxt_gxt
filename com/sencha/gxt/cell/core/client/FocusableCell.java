package com.sencha.gxt.cell.core.client;

import com.sencha.gxt.core.client.dom.XElement;

public interface FocusableCell {

  XElement getFocusElement(XElement parent);
  
}
