package com.sencha.gxt.cell.core.client.form;

import com.google.gwt.core.client.GWT;

public class RadioCell extends CheckBoxCell {

  public interface RadioAppearance extends CheckBoxAppearance {

  }

  public RadioCell() {
    this(GWT.<RadioAppearance> create(RadioAppearance.class));
  }

  public RadioCell(RadioAppearance appearance) {
    super(appearance);
  }

}
