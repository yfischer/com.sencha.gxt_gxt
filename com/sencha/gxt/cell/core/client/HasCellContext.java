package com.sencha.gxt.cell.core.client;

import com.google.gwt.dom.client.Element;

public interface HasCellContext {

  TargetContext getContext(int row, int column);
  
  Element findTargetElement(int row, int column);
  
}
