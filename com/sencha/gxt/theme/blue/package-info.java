/**
 * The default blue colored theme. This theme builds on the Base theme by applying blue images and styling rules to
 * the GXT components.
 */
package com.sencha.gxt.theme.blue;