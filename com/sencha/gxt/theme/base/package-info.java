/**
 * The default base theme for GXT. This theme applies enough CSS rules to give the components structure but does not
 * otherwise style components.
 */
package com.sencha.gxt.theme.base;