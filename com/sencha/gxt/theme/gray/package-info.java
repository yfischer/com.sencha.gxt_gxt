/**
 * The default gray colored theme. This theme builds on the Base theme by applying gray images and styling rules to
 * the GXT components.
 */
package com.sencha.gxt.theme.gray;
