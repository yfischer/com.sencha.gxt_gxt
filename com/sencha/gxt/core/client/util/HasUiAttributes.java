/**
 * 
 */
package com.sencha.gxt.core.client.util;

/**
 * Marker interface for types that may provide dynamic attribute parsing in
 * UiBinder.
 */
public interface HasUiAttributes {
}
