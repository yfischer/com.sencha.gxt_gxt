package com.sencha.gxt.core.client.gestures;

import com.google.gwt.dom.client.Element;

/**
 * Wrap a GWT Element object, allowing non-GWT test code to
 */
public interface IsElement {
  Element asElement();
}
