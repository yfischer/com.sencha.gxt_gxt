package com.sencha.gxt.core.client;

import com.google.gwt.i18n.client.Constants;

/**
 * Contains the current codes release information. Use {@link GXT#getVersion()} to get
 * an instance of this class.
 */
public interface Version extends Constants {
  /**
   * Returns the release name.
   * 
   * @return the release name
   */
  String getRelease();

  /**
   * Returns the build time.
   * 
   * @return the build time
   */
  String getBuildTime();


  /**
   * Returns the type of license used in this build, either 'gpl', 'commercial', or 'eval'.
   * @return the name of the license used in this build
   */
  String getLicenseType();
}
