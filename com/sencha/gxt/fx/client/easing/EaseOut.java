package com.sencha.gxt.fx.client.easing;

/**
 * {@link EasingFunction} that produces an easing at the end of the animation.
 */
public class EaseOut implements EasingFunction {

  @Override
  public double func(double n) {
    return Math.pow(n, 0.48);
  }

}
