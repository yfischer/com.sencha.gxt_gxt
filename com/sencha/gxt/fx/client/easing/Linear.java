package com.sencha.gxt.fx.client.easing;

/**
 * A one to one {@link EasingFunction}.
 * 
 */
public class Linear implements EasingFunction {

  @Override
  public double func(double n) {
    return n;
  }

}
