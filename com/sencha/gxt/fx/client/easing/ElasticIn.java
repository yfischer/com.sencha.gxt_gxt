package com.sencha.gxt.fx.client.easing;

/**
 * {@link EasingFunction} that produces an {@link Elastic} at the start of the
 * animation.
 */
public class ElasticIn extends Elastic {
}
