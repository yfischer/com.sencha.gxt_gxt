package com.sencha.gxt.fx.client.easing;

/**
 * {@link EasingFunction} that produces a {@link Bounce} at the end of the
 * animation.
 */
public class BounceOut extends Bounce {

}
