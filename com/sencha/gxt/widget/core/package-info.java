/**
 * The core widgets in the framework. These primitives are accepted by and used by many of the more complex panels and
 * layouts.
 */
package com.sencha.gxt.widget.core;