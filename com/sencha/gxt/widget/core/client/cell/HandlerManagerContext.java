package com.sencha.gxt.widget.core.client.cell;

import com.google.gwt.event.shared.HandlerManager;

public interface HandlerManagerContext {

  HandlerManager getHandlerManager();
}
