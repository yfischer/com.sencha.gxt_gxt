package com.sencha.gxt.widget.core.client.grid.editing;

/**
 * ClicksToEdit enumeration.
 */
public enum ClicksToEdit {
  /**
   * Editing start with one click.
   */
  ONE,
  /**
   * Editing starts with double click.
   */
  TWO
}
