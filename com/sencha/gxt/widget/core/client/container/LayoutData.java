package com.sencha.gxt.widget.core.client.container;

import com.sencha.gxt.core.client.util.HasUiAttributes;

/**
 * Marker interface for types that define layout parameters.
 */
public interface LayoutData extends HasUiAttributes {
}
